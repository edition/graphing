# README #

This repository contains an application called 'Graphing', which creates and views 2D graphs from data provided by the user. It uses the Direct2D API for drawing the graph, and functions from the Windows API to create the UI and to handle events.

## Build Instructions ##

First, please ensure that you have installed both the DirectX SDK released in 2010 and the Windows SDK (unless you are not using the express edition of Visual Studio).

The DirectX SDK released in June 2010 may be downloaded from here: http://www.microsoft.com/en-us/download/details.aspx?id=6812

Simply download the code from the master branch, and build the project in Visual Studio 2010.


## Project Suggestions ##

 - The application needs a better name