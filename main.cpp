#include <Windows.h>
#include <GdiPlus.h>
#include <d2d1.h>
#include <dwrite.h>
#include "resource.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

using namespace Gdiplus;

HWND hWindow;
HINSTANCE instance;

IDWriteFactory *d2WriteFactory;
IDWriteTextFormat *d2WriteTextFormat, *d2WriteTextDescFormat, *d2WriteTextInfoFormat;
ID2D1Factory *d2Factory;
ID2D1HwndRenderTarget *d2HwndRenderTarget;
HRESULT hrTarget;
ID2D1SolidColorBrush *lineBrush;
ID2D1SolidColorBrush *fontBrush;

const WCHAR textMessage[] = L"No Data Provided";

int gridSize = 12;

template<typename T>
void SafeRelease(T *&value)
{
	if (nullptr != value)
	{
		value->Release();
		value = nullptr;
	}
}

void AuthorBitmap(HWND handle)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(handle, &ps);

	Graphics *graphics = Graphics::FromHDC(hdc);
	HICON hIcon = LoadIcon(instance, MAKEINTRESOURCE(IDB_AUTHOR));
	Bitmap *bitmap = Bitmap::FromHBITMAP(LoadBitmap(instance, MAKEINTRESOURCE(IDB_AUTHOR)), NULL);

	graphics->DrawImage(bitmap, 10,10, 77, 72);

	EndPaint(handle, &ps);
}

INT_PTR CALLBACK AboutDialogEvents(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
		case WM_PAINT:
			AuthorBitmap(handle);
			break;
		case WM_INITDIALOG:
			return true;
		case WM_COMMAND:
			switch ( wParam ) 
			{
				case IDCANCEL:
				case IDOK:
					EndDialog(handle, 1);
					return 1;
			}
			break;
	}
	return 0;
}

void Paint()
{
	RECT rect;
	GetClientRect(hWindow, &rect);

	hrTarget = d2Factory->CreateHwndRenderTarget(D2D1::RenderTargetProperties(), 
		D2D1::HwndRenderTargetProperties(hWindow, D2D1::SizeU(rect.right - rect.left, rect.bottom - rect.top)), &d2HwndRenderTarget);
	
	if (SUCCEEDED(hrTarget)) {
		d2HwndRenderTarget->BeginDraw();

		d2HwndRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::White));

		//Create brushes
		d2HwndRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::LightGray), &lineBrush);
		d2HwndRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Gray), &fontBrush);
		
		//Draw gridlines
		d2HwndRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		D2D1_SIZE_F windowSize = d2HwndRenderTarget->GetSize();

		int midpoint = (windowSize.width / 2);

		for (int x=0; x < windowSize.width; x += gridSize) {
			if (x > (windowSize.width / 2) && x < (windowSize.width / 2) + gridSize) {
				d2HwndRenderTarget->DrawLine(D2D1::Point2F(x, 0), D2D1::Point2F(x, windowSize.height), fontBrush);
			} else {
				d2HwndRenderTarget->DrawLine(D2D1::Point2F(x, 0), D2D1::Point2F(x, windowSize.height), lineBrush);
			}
		}

		for (int y=0; y < windowSize.height; y += gridSize) {
			if (y > (windowSize.height / 2) && y < (windowSize.height / 2) + gridSize) {
				d2HwndRenderTarget->DrawLine(D2D1::Point2F(0, y), D2D1::Point2F(windowSize.width, y), fontBrush);
			} else {
				d2HwndRenderTarget->DrawLine(D2D1::Point2F(0, y), D2D1::Point2F(windowSize.width, y), lineBrush);
			}	
		}

		
		d2HwndRenderTarget->DrawTextA(L"Y", 1, d2WriteTextInfoFormat, D2D1::RectF((windowSize.width / 2) + 10, 0, rect.right, rect.bottom), fontBrush);
		d2HwndRenderTarget->DrawTextA(L"X", 1, d2WriteTextInfoFormat, D2D1::RectF(rect.right - gridSize, (windowSize.height / 2) - 10, rect.right - 10, rect.bottom), fontBrush);

		d2HwndRenderTarget->DrawTextA(L"-Y", 2, d2WriteTextInfoFormat, D2D1::RectF((windowSize.width / 2) + 10, rect.bottom - 20, rect.right, rect.bottom), fontBrush);
		d2HwndRenderTarget->DrawTextA(L"-X", 2, d2WriteTextInfoFormat, D2D1::RectF(0, (windowSize.height / 2) - 10, rect.right - 10, rect.bottom), fontBrush);

		d2HwndRenderTarget->DrawTextA(textMessage, ARRAYSIZE(textMessage), d2WriteTextFormat, D2D1::RectF(0, windowSize.height / 2 - 50, rect.right, rect.bottom), fontBrush);

		d2HwndRenderTarget->EndDraw();

		SafeRelease(d2HwndRenderTarget);
		SafeRelease(lineBrush);
		SafeRelease(fontBrush);
	}
}

LRESULT CALLBACK OnEvent(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {

		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;

		case WM_MOUSEWHEEL:
			if (GET_WHEEL_DELTA_WPARAM(wParam) > 0) {
				if (gridSize < 20) { gridSize += 2; }
			} else {
				if (gridSize >= 12) { gridSize -= 2; }
			}
			SendMessage(hWindow, WM_PAINT, 0, 0);
			break;
		case WM_SIZE:
		case WM_PAINT:
			Paint();
			break;
		case WM_COMMAND:
			if (LOWORD(wParam) == ID_HELPABOUT) {
				DialogBox(instance, MAKEINTRESOURCE(IDD_ABOUTDIALOG), hWindow, (DLGPROC)AboutDialogEvents);
			}
			break;
	}
	return DefWindowProc(handle, message, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR args, int nShowCmd)
{
	instance = hInstance;
	
	WNDCLASS windowClass;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hbrBackground = CreateSolidBrush(RGB(255,255,255));
	windowClass.hCursor = LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW));
	windowClass.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(IDI_APPLICATION));
	windowClass.hInstance = hInstance;
	windowClass.lpfnWndProc = &OnEvent;
	windowClass.lpszClassName = "Graph";
	windowClass.lpszMenuName = MAKEINTRESOURCE(IDR_APPMENU);
	windowClass.style = 0;
	RegisterClass(&windowClass);

	ULONG_PTR gdiToken;
	GdiplusStartupInput gdiStartupInput;

	GdiplusStartup(&gdiToken, &gdiStartupInput, NULL);

	hWindow = CreateWindow("Graph", "Graphing", WS_VISIBLE | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_SYSMENU | WS_THICKFRAME, 50, 50, 660, 520, NULL, NULL, hInstance, NULL);

	MSG message;

	HRESULT hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &d2Factory);
	DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(d2WriteFactory),
		reinterpret_cast<IUnknown **>(&d2WriteFactory));

	d2WriteFactory->CreateTextFormat(L"Segoe UI", NULL,
		DWRITE_FONT_WEIGHT_LIGHT,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		50.0f,
		L"en-us", 
		&d2WriteTextFormat);

	d2WriteFactory->CreateTextFormat(L"Segoe UI", NULL,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		18.0f,
		L"en-us", 
		&d2WriteTextDescFormat);

	d2WriteFactory->CreateTextFormat(L"Segoe UI", NULL,
		DWRITE_FONT_WEIGHT_BOLD,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		18.0f,
		L"en-us", 
		&d2WriteTextInfoFormat);

	d2WriteTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_CENTER);
	d2WriteTextDescFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_CENTER);

	if (!SUCCEEDED(hr)) {
		MessageBoxA(NULL, "Failed to initialize graphics", "Error", MB_ICONERROR);
		return 0;
	}

	while (GetMessage(&message, NULL, 0,0)) {
		TranslateMessage(&message);
		DispatchMessage(&message);
	}

	GdiplusShutdown(gdiToken);

	return 0;
}