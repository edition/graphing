//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Graph.rc
//
#define IDR_APPMENU                     101
#define IDD_ABOUTDIALOG                 102
#define IDB_AUTHOR                      104
#define ID_HELPABOUT                    40008
#define ID_FILENEW                      40009
#define ID_FILESAVE                     40010
#define ID_FILEEXIT                     40011
#define ID_EDITCOPY                     40012
#define ID_EDITPASTE                    40013
#define ID_HELPDOCUMENTATION            40014
#define ID_FILE_OPEN40015               40015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40016
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
